/*
 * Corps des fonctions definies dans queue.h.
*/

#include <stdlib.h>

#include "queue.h"


Queue new_queue(long capacity) {
    Queue queue;
    queue.capacity = capacity;
    queue.size = 0;
    queue.tab = (int *) malloc(sizeof(int) * capacity);
    queue.first = queue.tab;
    queue.last = queue.tab + capacity-1;
    return queue;
}

bool is_queue_empty(Queue * queue) {
    return queue->size == 0;
}

bool is_queue_full(Queue * queue) {
    return queue->size == queue->capacity;
}

void offer(Queue * queue, int element) {
    queue->size++;
    queue->last = queue->tab + (queue->last - queue->tab + 1) % queue->capacity;
    *queue->last = element;
}

int poll(Queue * queue) {
    int first = *queue->first;
    queue->size--;
    queue->first = queue->tab + (queue->first - queue->tab + 1) % queue->capacity;
    return first;
}

void free_queue(Queue * queue) {
    free(queue->tab);
}
#ifndef QUEUE_H
#define QUEUE_H

#include "stdbool.h"


/*
 * Definition de la structure de pile.
*/
typedef struct queue_t {
    long capacity; //Taille maximale de la file.
    long size; //Nombre d'elements dans la file.
    int * first; //Pointeur vers le debut de la file.
    int * last; //Pointeur vers la fin de la file.
    int * tab; //Pointeur vers la liste contigue.
} Queue;

/*
 * Cree une nouvelle file vide.
 * @param capacity la taille maximale de la file.
 * @return une nouvelle file vide.
*/
Queue new_queue(long capacity);

/*
 * Indique si une file est vide.
 * @param queue un pointeur vers la file.
 * @return si la file est vide.
*/
bool is_queue_empty(Queue * queue);

/*
 * Indique si une file est pleine.
 * @param queue un pointeur vers la file.
 * @return si la file est pleine.
*/
bool is_queue_full(Queue * queue);

/*
 * Ajoute un element dans une file.
 * @param queue un pointeur vers la file.
 * @param element l'element a ajouter.
*/
void offer(Queue * queue, int element);

/*
 * Supprime de la file l'element de tete et le renvoie.
 * @param queue un pointeur vers la file.
 * @return l'element de tete.
*/
int poll(Queue * queue);

/*
 * Libere une file.
 * @param queue un pointeur vers la file.
*/
void free_queue(Queue * queue);


#endif
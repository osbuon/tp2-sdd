/*
 * Corps des fonctions definies dans stack.h.
*/

#include <stdlib.h>

#include "stack.h"


Stack new_stack(long capacity) {
    Stack stack;
    stack.capacity = capacity;
    stack.head = -1;
    stack.tab = (StackType *) malloc(sizeof(StackType) * capacity);
    return stack;
}

bool is_stack_empty(Stack * stack) {
    return stack->head == -1;
}

bool is_stack_full(Stack * stack) {
    return stack->capacity == stack->head+1;
}

void push(Stack * stack, StackType element) {
    stack->head++;
    stack->tab[stack->head] = element;
}

StackType pop(Stack * stack) {
    StackType head = stack->tab[stack->head];
    stack->head--;
    return head;
}

void free_stack(Stack * stack) {
    free(stack->tab);
}
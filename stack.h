#ifndef STACK_H
#define STACK_H

#include "stdbool.h"


/*
 * Definition du type d'element contenu dans la pile.
*/
typedef int StackType;


/*
 * Definition de la structure de pile.
*/
typedef struct stack_t {
    long capacity; //Taille maximale de la pile.
    long head; //Indice de la tete de la pile.
    StackType * tab; //Pointeur vers la liste contigue.
} Stack;

/*
 * Cree une nouvelle pile vide.
 * @param capacity la taille maximale de la pile.
 * @return une nouvelle pille vide.
*/
Stack new_stack(long capacity);

/*
 * Indique si une pile est vide.
 * @param stack un pointeur vers la pile.
 * @return si la pile est vide.
*/
bool is_stack_empty(Stack * stack);

/*
 * Indique si une pile est pleine.
 * @param stack un pointeur vers la pile.
 * @return si la pile est pleine.
*/
bool is_stack_full(Stack * stack);

/*
 * Ajoute un element dans une pile.
 * @param stack un pointeur vers la pile.
 * @param element l'element a ajouter.
*/
void push(Stack * stack, StackType element);

/*
 * Supprime de la pile l'element de tete et le renvoie.
 * @param stack un pointeur vers la pile.
 * @return l'element de tete.
*/
StackType pop(Stack * stack);

/*
 * Libere une pile.
 * @param stack un pointeur vers la pile.
*/
void free_stack(Stack * stack);


#endif
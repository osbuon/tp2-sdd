#include <stdio.h>

#include "stack.h"
#include "queue.h"


void inverser(Stack * stack) {
    Queue queue = new_queue(stack->capacity);

    while (!is_stack_empty(stack))
        offer(&queue, pop(stack));
    
    while (!is_queue_empty(&queue))
        push(stack, poll(&queue));
    
    free_queue(&queue);
}

int main() {
    Stack stack = new_stack(10);

    for (int i = 1; i <= 10; i++)
        push(&stack, i);
    
    inverser(&stack);

    while (!is_stack_empty(&stack))
        printf("%d\n", pop(&stack));

    free_stack(&stack);

    return 0;
}